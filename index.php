<html>
<head>
    <title>Raycast engine</title>
    <script data-main="./scripts/main" src="./scripts/ext_libs/require.js"></script>
    <link rel="stylesheet" href="./css/style.css" />
    <link rel="stylesheet" href="./css/ui.css" />
    <link rel="stylesheet" href="./css/input_range.css" />
</head>
<body oncontextmenu="return false;">
    <span id="askInput" class="ui">
        <h1>Click to start</h1>
    </span>
    <span id="displays">
        <canvas id="render"></canvas>
        <canvas id="postprocess"></canvas>
    </span>

    <span id="uiContainer">
        <?php
            include 'uis/main_menu_ui.php';
            include 'uis/pause_ui.php';
        ?>
    </span>

    <span id="audioBank" muted="muted"></span>
    <span id="version">0.2</span>
</body>
</html>
