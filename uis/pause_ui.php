<span style="display:none;" id="pause" class="ui pause">
    <h1 id="title">
        Title
    </h1>
    <ul id="menu">
        <li id="resumeButton" class="uiButton">
            Resume
        </li>
        <li id="pauseOptionsButton" class="uiButton">
            Options
        </li>
        <li id="restartButton" class="uiButton">
            Restart
        </li>
        <li id="quitButton" class="uiButton">
            Quit
        </li>
    </ul>
</span>
<span style="display:none;" id="pauseOptions" class="ui pause">
    <h1 id="title">
        Options
    </h1>
    <ul id="menu">
        <li>
            Main Volume : <input id="pauseMainVolume" type="range" min="0" max="1" value=".5" step=".1"/>
        </li>
        <li>
            Music Volume : <input id="pauseMusicVolume" type="range" min="0" max="1" value=".5" step=".1"/>
        </li>
        <li>
            FX Volume : <input id="pauseFxVolume" type="range" min="0" max="1" value=".5" step=".1"/>
        </li>
        <li id="pauseQuitOptions" class="uiButton">
            Back
        </li>
    </ul>
</span>
<span style="display:none;" id="pauseConfirm" class="ui pause">
    <h1 id="title">
        Confirm ?
    </h1>
    <ul id="menu">
        <li id="yesButton" class="uiButton">
            Yes
        </li>
        <li id="noButton" class="uiButton">
            No
        </li>
    </ul>
</span>
