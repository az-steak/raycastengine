<span style="display:none;" id="mainMenu" class="ui mainMenu">
    <h1 id="title">
        Raycast Engine
    </h1>
    <ul id="menu">
        <li id="newGameButton" class="uiButton">
            New game
        </li>
        <li id="loadButton" class="uiButton">
            Load game
        </li>
        <li id="mainOptionsButton" class="uiButton">
            Options
        </li>
        <li id="creditsButton" class="uiButton">
            Credits
        </li>
    </ul>
</span>
<span style="display:none;" id="levelSelection" class="ui levelSelection">
    <h4 id="subtitle">
        Select Level
    </h4>
    <ul id="selectMenu">

        <!-- <li class="uiButton levelSelect" data-filename="Entrance">
            Entrance
        </li>
        <li class="uiButton levelSelect" data-filename="EntranceHugeTest">
            EntranceHuge
        </li>
        <li class="uiButton levelSelect" data-filename="level0">
            LevelTest2
        </li>
        <li class="uiButton levelSelect" data-filename="level1">
            LevelTest3
        </li>
        <li class="uiButton levelSelect" data-filename="level2">
            LevelTest4
        </li>
        <li class="uiButton levelSelect" data-filename="NewLevel">
            LevelTest5
        </li> -->
    </ul>
    <span id="quitLevelSelect" class="uiButton">Back</span>
</span>
<span style="display:none;" id="mainOptions" class="ui pause">
    <h1 id="title">
        Options
    </h1>
    <ul id="menu">
        <li>
            Main Volume : <input id="mainVolume" type="range" min="0" max="1" value=".5" step=".1"/>
        </li>
        <li>
            Music Volume : <input id="musicVolume" type="range" min="0" max="1" value=".5" step=".1"/>
        </li>
        <li>
            FX Volume : <input id="fxVolume" type="range" min="0" max="1" value=".5" step=".1"/>
        </li>
        <li id="mainQuitOptions" class="uiButton">
            Back
        </li>
    </ul>
</span>
<span style="display:none" id="imgMelt" class="ui"></span>
