define([], function () {


    function Color (r = 1, g = 1, b = 1, a = 1) {
        this.r = r
        this.g = g
        this.b = b
        this.a = a
    }






    Color.lerp = function (colorA, colorB, t) {
        return Color.blue
    }





    ///// CSTS

    Color.red = function () {
        return new Color(1, 0, 0, 1)
    }

    Color.blue = function () {
        return new Color(0, 0, 1, 1)
    }

    Color.green = function () {
        return new Color(0, 1, 0, 1)
    }

    Color.black = function () {
        return new Color(0, 0, 0, 1)
    }

    Color.white = function () {
        return new Color(1, 1, 1, 1)
    }


    return Color

})
