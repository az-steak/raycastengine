define([], function () {

    function Vector2 (x = 0, y = 0) {
        this.x = x
        this.y = y
    }


    // INSTANCES OPS

    Vector2.prototype.normalize = function () {
        var norm = this.magnitude()
        this.x /= norm
        this.y /= norm
    }

    Vector2.prototype.normalized = function () {
        var norm = Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
        return new Vector2(this.x / norm, this.y / norm)
    }

    Vector2.prototype.magnitude = function () {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2))
    }

    Vector2.prototype.add = function (v) {
        this.x += v.x
        this.y += v.y
    }

    Vector2.prototype.minus = function (v) {
        this.x = this.x - v.x
        this.y = this.y - v.y
    }

    Vector2.prototype.multiply = function (k) {
        this.x *= k
        this.y *= k
    }


    // STATICS OPS

    Vector2.add = function (v1, v2) {
        return new Vector2(v1.x + v2.x, v1.y + v2.y)
    }

    Vector2.minus = function (v1, v2) {
        return new Vector2(v1.x - v2.x, v1.y - v2.y)
    }

    Vector2.dot = function (v1, v2) {
        return v1.x * v2.x + v1.y * v2.y
    }

    Vector2.cross = function (v1, v2) {
        return v1.x * v2.y + v1.y * v2.x
    }

    Vector2.normalized = function (v) {
        var norm = Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2))
        return new Vector2(v.x / norm, v.y / norm)
    }

    Vector2.distance = function (v1, v2) {
        return Math.sqrt(Math.pow(v1.x - v2.x, 2) + Math.pow(v1.y - v2.y, 2))
    }

    Vector2.lerp = function (v1, v2, ratio) {
        ratio = Math.min(Math.max(ratio, 0), 1)

        return new Vector2(
            v1.x + ratio * (v2.x - v1.x),
            v1.y + ratio * (v2.y - v1.y)
        )
    }

    Vector2.project = function (vector, projectVector) {
        projectVector.normalize()
        var k = Vector2.dot(vector, projectVector) / Vector2.dot(projectVector, projectVector)

        return new Vector2(projectVector.x * k, projectVector.y * k)
    }



    // CONSTANTS

    Vector2.zero = function () {
        return new Vector2()
    }

    Vector2.one = function () {
        return new Vector2(1, 1)
    }

    Vector2.up = function () {
        return new Vector2(0, 1)
    }

    Vector2.down = function () {
        return new Vector2(0, -1)
    }

    Vector2.left = function () {
        return new Vector2(-1, 0)
    }

    Vector2.right = function () {
        return new Vector2(1, 0)
    }

    /////////////////

    return Vector2
})
