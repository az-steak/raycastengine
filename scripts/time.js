define([], function () {
    var time = 0
    var deltaTime = 0



    return {
        time: function () {
            return time
        },
        deltaTime: function () {
            return deltaTime
        },
        setDeltaTime: function (value) {
            deltaTime = value
            time += value
        }
    }
})
