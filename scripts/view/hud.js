define([], function () {

    function HUD () {
        this.backgroundTexture = $('<img src="res/textures/UI/HUDBackground.png"></img>')[0]

        this.heightRatio = 0.15
        this.health = 100
        this.ammo = 10
    }

    HUD.prototype.render = function (ctx) {
        var resolution = {
            x: ctx.canvas.width,
            y: ctx.canvas.height
        }
        var height = ctx.canvas.height * this.heightRatio
        var textSize = Math.floor(height * 0.3)

        ctx.drawImage(this.backgroundTexture, 0, resolution.y - height, resolution.x, height)

        ctx.font = textSize + "px doom"
        ctx.fillStyle = "rgb(241, 0, 0)"
        ctx.textAlign = "center"



        this.renderText(ctx, this.health, {x: resolution.x / 2, y: resolution.y - height / 2 + textSize / 3})
        this.renderText(ctx, this.ammo, {x: resolution.x / 8, y: resolution.y - height / 2 + textSize / 3})
    }

    HUD.prototype.renderText = function (ctx, text, position) {
        ctx.strokeStyle = "rgb(100, 0, 0)"
        ctx.lineWidth = 6
        ctx.strokeText(text, position.x, position.y)
        ctx.fillText(text, position.x, position.y)
    }


    return HUD
})
