define(['time', 'math/Vector2', 'entities/base_entity', 'view/pp/meltPicture', 'utils/pathfinding'], function (Time, Vector2, Entity, PP, Pathfinding) {

    var minimapRes = 200
    var errorTextures = {}
    var ppData
    var packageData

    var debugPath = []

    function Raycast (renderCanvas, ppCanvas, resolution, level) {
        renderCanvas.attr('height', resolution.y + 'px')
        renderCanvas.attr('width', resolution.x + 'px')
        ppCanvas.attr('height', resolution.y + 'px')
        ppCanvas.attr('width', resolution.x + 'px')

        this.resolution = resolution
        this.raycastResolution = resolution.x
        this.level  = level
        this.renderCanvas = renderCanvas
        this.ctx    = renderCanvas[0].getContext('2d')
        this.ppctx  = ppCanvas[0].getContext('2d')

        ppData = this.ppctx.createImageData(resolution.x, resolution.y)


        this.zBuffer = []

        var sizeFactor = 1.8
        this.resolutionFactor = new Vector2(
            this.resolution.x / 1600 * sizeFactor,
            this.resolution.y / 900 * sizeFactor
        )

        this.minimap = $('<canvas id="minimap"></canvas')
        this.minimap.attr('height', minimapRes + 'px')
        this.minimap.attr('width', minimapRes + 'px')

        $('#displays').append(this.minimap)
    }

    Raycast.setPackageData = function (package) {
        packageData = package
    }


    Raycast.prototype.loadTextures = function (level) {
        var ressources = level.ressources
        var entities   = level.entities
        this.textures = {}
        errorTextures["errTex"] = $('<img src="./res/textures/ErrTex.png"></img>')[0]
        errorTextures["errDat"] = $('<img src="./res/textures/ErrDat.png"></img>')[0]
        this.background = $('<img id="background" src="./res/textures/' + level.backdrop + '.bmp"></img>')[0]

        for (var i in ressources) {
            if (ressources[i].texture) {
                // var path = './res/' + ressources[i].texture + '.png'
                // this.textures[i] = $('<img src="' + data + '"></img>')[0]

                var texture = new Image(128, 128)
                var textureData = packageData.textures.walls[ressources[i].texture]
                var data = 'data:image/png;base64,' + textureData

                texture.src = data
                this.textures[i] = texture
            }
        }
    }

    var frame = 0

    Raycast.prototype.render = function (level, player) {
        if (frame % 4 == 0) {
            Entity.sortEntities()
        }
        frame++

        this.clearScreens()
        this.drawBackground(player)
        this.renderMiniMap(level, player)

        this.renderCeiling(player)
        this.raycast(level, player)
        this.renderEntities(level, player)

        // this.renderPostProcess()
        this.renderWeapon(player)
        player.hud.render(this.ctx)
    }


    Raycast.prototype.renderWeapon = function (player) {
        var texture = player.weapon.getTexture()
        var height = (Math.cos(player.bobbingCount * 0.5) + 1) * 4 - this.resolution.y * player.hud.heightRatio / 2 + player.weaponOffsetY
        var offsetX = Math.cos(player.bobbingCount * 0.25 + Math.PI / 2) * 10
        this.ctx.drawImage(texture, offsetX, height, this.resolution.x, this.resolution.y + height)
    }



    Raycast.prototype.renderPostProcess = function () {
        var color
        var index

        for (var y = 0; y < this.resolution.y; y++) {
            for (var x = 0; x < this.resolution.x; x++) {
                index = (x + y * this.resolution.x) * 4

                color = PP({x,y}, this.zBuffer[x], this.resolution)

                ppData.data[index] = color.r
                ppData.data[index + 1] = color.g
                ppData.data[index + 2] = color.b
                ppData.data[index + 3] = color.a
            }
        }


        this.ppctx.putImageData(ppData, 0, 0)
    }


    Raycast.prototype.drawBackground = function (player) {
        var angle = player.orientation / (Math.PI * 2)
        var scrollSpeed = 6

        var offsetX = angle * this.resolution.x * -scrollSpeed

        for (var i = -1; i < scrollSpeed + 1; i++) {
            this.ctx.drawImage(this.background, offsetX + i * this.resolution.x, 0, this.resolution.x, this.resolution.y)
        }
    }

    Raycast.prototype.raycast = function (level, player) {
        var firstAngle = player.orientation - player.fov / 2
        var angleStep = player.fov / this.raycastResolution

        var rayAngle
        var rayDir = {}
        var rayPos = player.getCoords() // ray origin
        var mapCoords // coord in map units (integers)
        var deltaDist = {} // distance between 2 x-collison & 2 y-collision
        var steps
        var sideDist
        var textureXPortion
        var hit = false
        var side = 0

        for (var x = 0; x < this.raycastResolution; x++) {
            mapCoords = {
                x: Math.floor(rayPos.x),
                y: Math.floor(rayPos.y)
            }
            rayAngle = firstAngle + x * angleStep
            rayDir.x = Math.cos(rayAngle)
            rayDir.y = Math.sin(rayAngle)

            deltaDist.x = Math.sqrt(1 + (rayDir.y ** 2) / (rayDir.x ** 2))
            deltaDist.y = Math.sqrt(1 + (rayDir.x ** 2) / (rayDir.y ** 2))

            var data = getStepAndSideDist({
                rayDir: rayDir,
                rayPos: rayPos,
                mapCoords: mapCoords,
                deltaDist: deltaDist
            })
            steps    = data.steps
            sideDist = data.sideDist

            var hitBlock
            // look for hit

            var drawList = []

            while (!hit) {
                if (sideDist.x < sideDist.y) {
                    sideDist.x += deltaDist.x
                    mapCoords.x += steps.x
                    side = 0
                } else {
                    sideDist.y += deltaDist.y
                    mapCoords.y += steps.y
                    side = 1
                }
                hitBlock = level.getTileAt(mapCoords.x, mapCoords.y)

                if (!hitBlock.walkable) {
                    drawList.push(getColumnParams({
                        resolution:        this.resolution,
                        raycastResolution: this.raycastResolution,
                        player,
                        side,
                        mapCoords,
                        rayPos,
                        rayDir,
                        rayAngle,
                        steps,
                        x,
                        deltaDist,
                        hitBlock
                    }))

                    if (!hitBlock.transparent) {
                        break;
                    }
                }
            }

            this.zBuffer[x] = drawList[0].distance

            for (var i = drawList.length; i > 0; i--) {
                this.drawTexturedColumn(drawList.pop())
            }
        }
    }

    function getColumnParams (params) {
        var distance
        var columnWidth = params.resolution.x / params.raycastResolution

        var blocRelativeX

        if (params.side === 0) {
            distance = (params.mapCoords.x - params.rayPos.x + (1 - params.steps.x) / 2) / params.rayDir.x
            blocRelativeX = (params.player.y + distance * params.rayDir.y)
        } else {
            distance = (params.mapCoords.y - params.rayPos.y + (1 - params.steps.y) / 2) / params.rayDir.y
            blocRelativeX = (params.player.x + distance * params.rayDir.x)
        }

        blocRelativeX = blocRelativeX - Math.floor(blocRelativeX)


        return {
            columnWidth: columnWidth + 1,
            distance:    fixDistance(distance, params.x / params.raycastResolution), //* Math.cos(params.player.orientation - params.rayAngle),
            blockX: blocRelativeX,
            columnX: params.x * columnWidth,
            x: params.x,
            surfaceType: params.hitBlock.name,
            color: params.hitBlock.color,
            elevation: params.hitBlock.elevation || 0
        }
    }


    function getStepAndSideDist (params) {
        var rayDir = params.rayDir
        var rayPos = params.rayPos
        var mapCoords = params.mapCoords
        var deltaDist = params.deltaDist


        var steps = {}
        var sideDist = {}

        if (rayDir.x < 0) {
            steps.x = -1
            sideDist.x = (rayPos.x - mapCoords.x) * deltaDist.x;
        } else
        {
            steps.x = 1;
            sideDist.x = (mapCoords.x + 1.0 - rayPos.x) * deltaDist.x;
        }

        if (rayDir.y < 0)
        {
            steps.y = -1;
            sideDist.y = (rayPos.y - mapCoords.y) * deltaDist.y;
        } else
        {
            steps.y = 1;
            sideDist.y = (mapCoords.y + 1.0 - rayPos.y) * deltaDist.y;
        }

        return {
            steps: steps,
            sideDist: sideDist
        }
    }


    Raycast.prototype.drawColumn = function (params) {
        var height = this.resolution.y / params.distance
        var color = getColumnColor({
            color: params.color,
            distance: params.distance
        })

        this.ctx.fillStyle = params.color
        this.ctx.fillRect(params.x, this.resolution.y / 2 - height / 2, params.columnWidth, height)
    }


    Raycast.prototype.drawTexturedColumn = function (params) {
        var textureObject = $(this.textures[params.surfaceType])[0]
        var texture
        // console.log(textureObject);

        if (textureObject == null) {
            texture = errorTextures["errDat"]
        } else if (textureObject.width == 0) {  // if width == 0 means img doesn't exist (probably)
            texture = errorTextures["errTex"]
        } else {
            texture = textureObject
        }


        var height = this.resolution.y / params.distance
        var textureX = params.blockX * texture.naturalWidth
        var offsetY = 0

        var displayParams = {
            dx: params.columnX,
            dy: this.resolution.y / 2 - height / 2 - offsetY,
            sx: textureX,
            sy: 0,
            sWidth: 1,
            sHeight: texture.naturalHeight,
            dWidth: params.columnWidth,
            dHeight: height
        }

        this.ctx.drawImage(texture,
            displayParams.sx, displayParams.sy,
            displayParams.sWidth, displayParams.sHeight,
            displayParams.dx, displayParams.dy,
            displayParams.dWidth, displayParams.dHeight
        )
    }

    Raycast.prototype.renderGround = function (player) {

    }

    Raycast.prototype.renderCeiling = function (player) {

    }

    Raycast.prototype.renderEntities = function (level, player) {
        var distance
        var entity
        var texture
        var displayPosition = new Vector2()
        var size = new Vector2()
        var viewDir = player.getForward()
        var angle // angle between player orientation and player-entity vector

        for (var i in Entity.entities) {
            entity = Entity.entities[i]

            if (entity.getTexture() == null) {
                continue
            }

            angle = player.getAngleInView(entity)

            if (angle > player.fov / 2 + 0.2|| angle < -player.fov / 2 - 0.2) {
                continue
            }


            distance = Math.sqrt(Math.pow(entity.x - player.x, 2) + Math.pow(entity.y - player.y, 2))
            var difference = Vector2.minus(direction, viewDir)

            if (Vector2.dot(direction, new Vector2(-viewDir.y, viewDir.x)) < 0) {
                angle *= -1
            }

            displayPosition.x = Math.sin(angle) * this.resolution.x + this.resolution.x / 2

            distance = fixDistance(distance, displayPosition.x / this.resolution.x)

            texture = entity.getTexture()
            size.x = texture.width / (3 / this.resolutionFactor.x)
            size.y = texture.height / (3 / this.resolutionFactor.y)
            displayPosition.y = this.resolution.y / 2 + this.resolution.y / distance / 2 - size.y / distance / 2
            entity.isVisible = this.drawSprite(texture, displayPosition, size, distance)
        }
    }

    Raycast.prototype.drawSprite = function (texture, position, size, distance) { // draw sprite and return false if sprite is entirely hidden
        size.x /= distance
        size.y /= distance

        var processedPosition = new Vector2(
            position.x - size.x / 2,
            position.y - size.y / 2
        )

        var visible = false

        for (var x = 0; x < size.x; x++) { // draw sprite column by column
            var screenX = position.x - size.x / 2 + x

            if (!(this.zBuffer[Math.round(screenX)] != undefined && distance > this.zBuffer[Math.round(screenX)])) {
                this.ctx.drawImage(texture,
                    x / size.x * texture.width, 0,
                    1, texture.height,
                    screenX, position.y - size.y / 2,
                    1, size.y
                )

                if (x == Math.round(size.x / 2)) {
                    visible = true
                }
            }

        }

        return visible
    }

    function normalizeVector (vector) {
        var norm = Math.sqrt(Math.pow(vector.x, 2) + Math.pow(vector.y, 2))
        vector.x /= norm
        vector.y /= norm
        return vector
    }

    function fixDistance (distance, xRatio) {
        return distance * Math.cos((xRatio - 0.5))
    }

    Raycast.prototype.renderMiniMap = function (level, player) {
        var ctx = this.minimap[0].getContext('2d')
        var blockSize = minimapRes / level.size

        ctx.strokeStyle = 'rgb(17, 17, 17)'
        ctx.lineWidth = 2
        ctx.fillStyle = '#a55'
        ctx.beginPath()
        for (var y = 0; y < level.size; y++) {
            for (var x = 0; x < level.size; x++) {
                if (!level.getTileAt(x, y).walkable) {
                    ctx.rect(x * blockSize, y * blockSize, blockSize, blockSize)
                }
            }

        }
        ctx.fill()
        ctx.stroke()

        // draw player
        ctx.fillStyle = '#a00'
        ctx.beginPath();
        ctx.arc(player.x * blockSize, player.y * blockSize, minimapRes / 40, 0, 2 * Math.PI)
        ctx.fill()

        // draw look direction
        var look = {
            x: player.x + Math.cos(player.orientation),
            y: player.y + Math.sin(player.orientation)
        }

        ctx.beginPath()
        ctx.moveTo(player.x * blockSize, player.y * blockSize)
        ctx.lineTo(look.x * blockSize  , look.y * blockSize)
        ctx.stroke()

        // debug draw path
        if (debugPath.length > 0) {
            ctx.strokeStyle = 'rgb(255, 0, 0)'
            ctx.lineWidth = 5
            ctx.beginPath()
            ctx.moveTo(debugPath[0].x * blockSize, debugPath[0].y * blockSize)

            for (var i = 0; i < debugPath.length; i++) {
                ctx.lineTo(debugPath[i].x * blockSize, debugPath[i].y * blockSize)
            }

            ctx.stroke()
        }
    }

    Raycast.prototype.clearScreens = function () {
        this.ctx.clearRect(0, 0, this.resolution.x, this.resolution.y)
        this.minimap[0].getContext('2d').clearRect(0, 0, minimapRes, minimapRes)
    }

    Raycast.prototype.DebugPathfinding  = function (startCoord, endCoord) {
        debugPath = Pathfinding.findPath(startCoord, endCoord)
    }


    return Raycast
})
