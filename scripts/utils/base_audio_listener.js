define([], function () {

    var instance

    var sinPitch = 0
    var cosPitch = 1

    function getInstance() {
        return instance
    }

    function addCapabilities (object) {
        object.x = object.x || 0
        object.y = object.y || 0

        object.orientation = object.orientation || 0


        object.v3Forward = function (angleOffset = 0) {
            var orientation = object.orientation

            var sinYaw = Math.sin(orientation)
            var cosYaw = Math.cos(orientation)

            return {
                x: cosPitch * cosYaw,
                y: cosPitch * sinYaw,
                z: -sinPitch
            }
        }


        object.v3Up = function () {
            return {
                x: 0,
                y: 1,
                z: 0
            }
        }


        instance = object
    }


    return {addCapabilities, getInstance}
})
