define(['jquery', 'utils/base_audio_listener'], function ($, AudioListener) {

    var AudioContext = window.AudioContext || window.webkitAudioContext
    var audioCtx
    var listener

    var loadedSounds = {}
    var loadedSpacializedSounds = {}
    var bgm

    var fxEffects = []
    var bgmEffects = []

    var mainGainNode
    var musicGainNode
    var fxGainNode

    function init () {
        audioCtx = new AudioContext()
        listener = audioCtx.listener
        listener.setPosition(0, 0, 0)
        audioCtx.resume()

        mainGainNode = new GainNode(audioCtx)
        musicGainNode = new GainNode(audioCtx)
        fxGainNode = new GainNode(audioCtx)

        bgmEffects.push(musicGainNode)
        fxEffects.push(fxGainNode)
    }

    function update () {
        updateListener()
    }



    function playSound (event, volume = 1) {
        var audioElement = getSoundObject(event, true).audioElement

        audioElement.volume = volume
        audioElement.play()
    }


    function playSoundAtLocation (event, v2Position, volume = 1) {
        var soundObject = getSpacializedSoundObject(event, v2Position)

        soundObject.spacializationNode.positionX.value = v2Position.x
        soundObject.spacializationNode.positionZ.value = v2Position.y
        soundObject.audioElement.volume = volume

        soundObject.audioElement.play()
    }


    function playMusic (event, loop = false) {
        if (bgm != undefined && !bgm.audioElement.paused) {
            bgm.audioElement.pause()
        }

        bgm = getSoundObject(event)
        bgm.audioElement.play()


        if (loop) {
            bgm.audioElement.addEventListener("ended", function () {
                console.log("ended");
                playMusic(event)
            })
        }
    }


    function resumeMusic() {
        if (bgm != undefined && bgm.audioElement.paused) {
            bgm.audioElement.play()
        }
    }


    function pauseMusic () {
        if (bgm != undefined) {
            bgm.audioElement.pause()
        }
    }

    function stopMusic () {
        if (bgm != undefined) {
            bgm.audioElement.pause()
            bgm.audioElement.currentTime = 0
        }
    }


    function getSoundObject (event, isFX) {
        if (loadedSounds[event] == undefined) {
            loadedSounds[event] = [isFX ? createSoundObject(event) : createMusicObject(event)]
        }

        for (soundObject of loadedSounds[event]) {
            if (soundObject.audioElement.paused) {
                return soundObject
            }
        }

        return addSoundInPool(event, isFX)
    }


    function getSpacializedSoundObject (event, position) {
        if (loadedSpacializedSounds[event] == undefined) {
            loadedSpacializedSounds[event] = [createSoundObject(event, position)]
        }

        for (soundObject of loadedSpacializedSounds[event]) {
            if (soundObject.audioElement.paused) {
                return soundObject
            }
        }

        return addSpacializedSoundInPool(event, position)
    }


    function addSoundInPool (event, isFX) {
        var eventObjects = loadedSounds[event]
        var newEventObject = isFX ? createSoundObject(event) : createMusicObject(event)
        eventObjects.push(newEventObject)

        return newEventObject
    }

    function addSpacializedSoundInPool (event, position) {
        var eventObjects = loadedSpacializedSounds[event]
        var newEventObject = createSoundObject(event, position)

        eventObjects.push(newEventObject)

        return newEventObject
    }


    function createSoundObject (event, position) {
        var path = getEventPath(event)
        var audioElement = $("<audio src='" + path + "' crossorigin='anonymous'></audio>")
        var track = audioCtx.createMediaElementSource(audioElement[0])

        if (position != undefined) {
            var spacializationNode = getSpacializationNode(track, position, 0)
            track = track.connect(spacializationNode)
        }


        track = applyConnects(track, fxEffects).connect(audioCtx.destination)
        // track.connect(audioCtx.destination)

        $("#audioBank").append(audioElement)

        return {
            audioElement: audioElement[0],
            isSpacialized : position != undefined,
            track,
            spacializationNode
        }
    }

    function createMusicObject (event) {
        var path = getEventPath(event)
        var audioElement = $("<audio src='" + path + "' crossorigin='anonymous'></audio>")
        var track = audioCtx.createMediaElementSource(audioElement[0])

        track = applyConnects(track, bgmEffects).connect(audioCtx.destination)

        $("#audioBank").append(audioElement)

        return {
            audioElement: audioElement[0],
            isSpacialized : false,
            track,
            spacializationNode: undefined
        }
    }


    function getEventPath (event) {
        return 'res/sounds/' + event + '.mp3'
    }


    function applyConnects (track, effects) {
        var connectedTrack = track

        for (effect of effects) {
            connectedTrack = connectedTrack.connect(effect)
        }

        return connectedTrack.connect(mainGainNode)
    }

    function getSpacializationNode (track, position, orientation) {
        var audioListener = AudioListener.getInstance()
        const pannerModel = 'HRTF';
        const innerCone = 360;
        const outerCone = 360;
        const outerGain = .4;
        const distanceModel = 'exponential';
        const maxDistance = 10;
        const refDistance = 5;
        const rollOff = 1;
        // let's use the class method for creating our panner node and pass in all those parameters we've set.
        const panner = new PannerNode(audioCtx, {
        	panningModel: pannerModel,
        	distanceModel: distanceModel,
        	positionX: position.x,
        	positionY: 0,
        	positionZ: position.y,
        	orientationX: 0,
        	orientationY: orientation,
        	orientationZ: 0,
        	refDistance: refDistance,
        	maxDistance: maxDistance,
        	rolloffFactor: rollOff,
        	coneInnerAngle: innerCone,
        	coneOuterAngle: outerCone,
        	coneOuterGain: outerGain
        })

        return panner
    }


    /// SPACIALIZATION

    function updateListener () {
        var audioListener = AudioListener.getInstance()
        var forward = audioListener.v3Forward()
        var up = audioListener.v3Up()

        var targetTime = audioCtx.currentTime + 0.1


        listener.positionX.linearRampToValueAtTime(audioListener.x, targetTime)
        listener.positionZ.linearRampToValueAtTime(audioListener.y, targetTime)

        listener.forwardX.linearRampToValueAtTime(forward.x, targetTime)
        listener.forwardY.linearRampToValueAtTime(0, targetTime)
        listener.forwardZ.linearRampToValueAtTime(forward.y, targetTime)

        listener.upX.linearRampToValueAtTime(0, targetTime)
        listener.upY.linearRampToValueAtTime(1, targetTime)
        listener.upZ.linearRampToValueAtTime(0, targetTime)
    }



    /// VOLUMES

    function setMainVolume (newValue) {
        mainGainNode.gain.setValueAtTime(newValue, audioCtx.currentTime)
    }

    function setMusicVolume (newValue) {
        musicGainNode.gain.setValueAtTime(newValue, audioCtx.currentTime)
    }

    function setFXVolume (newValue) {
        fxGainNode.gain.setValueAtTime(newValue, audioCtx.currentTime)
    }

    return {
        init,
        update,
        playSound,
        playSoundAtLocation,
        playMusic,
        pauseMusic,
        resumeMusic,
        stopMusic,
        setMainVolume,
        setMusicVolume,
        setFXVolume
    }
})
