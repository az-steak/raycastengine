define(['math/Vector2'], function (Vector2) {

    var grid
    var gridSize
    var components

    var openList = []
    var closedList = []

    function init (level) {
        grid = level.map
        gridSize = level.size
        components = level.ressources
    }

    function getCellCoordsFromWorldCoords (worldCoord) {
        return new Vector2(Math.floor(worldCoord.x), Math.floor(worldCoord.y))
    }

    function getGridIndexFromCoords (cellCoord) {
        return Math.floor(cellCoord.x) + Math.floor(cellCoord.y) * gridSize
    }

    function getCoordsFromGridIndex (index) {
        return new Vector2(index % gridSize, Math.floor(index / gridSize))
    }

    function getCellPredictScore (cellObject, cellObjectTarget) {
        return getCellMovedScore(cellObject) + getCellHeuristicScore(cellObject.position, cellObjectTarget.position)
    }

    function getCellHeuristicScore (from, to) {
        return Math.abs(from.x - to.y) + Math.abs(from.x - to.y)
    }

    function getCellMovedScore (cellObject) {
        var score = 0

        while (cellObject.parent != null) {
            score++
            cellObject = cellObject.parent
        }

        return score
    }

    function getCellAt (index) {
        return components[grid[index]]
    }

    function getAccessiblesCellsFrom (v2Origin, maxRange) {
        var cellCoord = getCellCoordsFromWorldCoords(v2Origin)
        closedList.length = 0
        exploreCellsFrom(cellCoord, maxRange + 1)
        var result = [...closedList]
        closedList.length = 0

        return result
    }

    function exploreCellsFrom (cellCoord, maxRange) {
        var cell = getCellAt(getGridIndexFromCoords(cellCoord))

        if (maxRange > 0 && cell.walkable && !isCellInList(cellCoord, closedList)) {
            closedList.push(cellCoord)

            var topCell = Vector2.add(cellCoord, {x: 0, y: -1})
            var leftCell = Vector2.add(cellCoord, {x: -1, y: 0})
            var rightCell = Vector2.add(cellCoord, {x: 1, y: 0})
            var bottomCell = Vector2.add(cellCoord, {x: 0, y: 1})

            exploreCellsFrom(topCell, maxRange - 1)
            exploreCellsFrom(leftCell, maxRange - 1)
            exploreCellsFrom(rightCell, maxRange - 1)
            exploreCellsFrom(bottomCell, maxRange - 1)
        }
    }

    function getCellWithLowestPredictScore () {
        var indexMin = 0
        var minScore = openList[0].g + openList[0].f

        for (var i in openList) {
            var cellObject = openList[i]
            var score = cellObject.f + cellObject.g

            if (score < minScore) {
                minScore = score
                indexMin = i
            }
        }

        var bestCell = openList.splice(indexMin, 1)[0]
        closedList.push(bestCell)

        return bestCell
    }

    function createCellObject (index) {
        var cell = getCellAt(index)
        var position = getCoordsFromGridIndex(index)

        return {
            parent: null,
            cell,
            position,
            f: 0,
            g: 0
        }
    }

    function getNeighbours (cellObject) {
        var cellIndex = getGridIndexFromCoords(cellObject.position)

        return [
            createCellObject(cellIndex - gridSize),
            createCellObject(cellIndex - 1),
            createCellObject(cellIndex + 1),
            createCellObject(cellIndex + gridSize)
        ]
    }

    function areCellsSame (coordA, coordB) {
        return coordA.x == coordB.x && coordA.y == coordB.y
    }

    function isCellInList (cellObject, list) {
        var result = false

        for (cell of list) {
            if (cellObject.x == cell.x && cellObject.y == cell.y) {
                result = true
                break
            }
        }

        return result
    }

    function isCellObjectInList (cellObject, list) {
        var result = false

        for (cell of list) {
            if (cellObject.position.x == cell.position.x && cellObject.position.y == cell.position.y) {
                result = true
                break
            }
        }

        return result
    }

    function randomPosInCell (cellCoord) {
        var random = .3
        return new Vector2(cellCoord.x + randomBetween(random, 1 - random), cellCoord.y + randomBetween(random, 1 - random))
    }

    function randomBetween (min, max) {
        return min + Math.random() * (max - min)
    }

    function constructPath (startCoord, endCoord) {
        var currentCell = closedList[closedList.length - 1]
        var path = [endCoord]

        while (currentCell.parent != null) {
            currentCell = currentCell.parent
            path.push(randomPosInCell(currentCell.position))
        }

        return path.reverse()
    }

    function findPath (from, to) {
        openList.length = 0
        closedList.length = 0

        var startCellCoord = getCellCoordsFromWorldCoords(from)
        var targetCellCoord = getCellCoordsFromWorldCoords(to)
        var startCell = getCellAt(getGridIndexFromCoords(startCellCoord))

        openList.push({
            parent: null,
            cell: startCell,
            position: startCellCoord,
            f: getCellHeuristicScore(startCellCoord, targetCellCoord),
            g: 0
        })

        while (openList.length > 0) {
            var bestCell = getCellWithLowestPredictScore()

            if (areCellsSame(bestCell.position, targetCellCoord)) {
                break
            }
            var neighbours = getNeighbours(bestCell)

            for (cellObject of neighbours) {
                if (!isCellObjectInList(cellObject, closedList) && cellObject.cell.walkable) {
                    cellObject.parent = bestCell
                    cellObject.g = getCellMovedScore(cellObject)
                    cellObject.f = cellObject.g + getCellHeuristicScore(cellObject.position, targetCellCoord)

                    openList.push(cellObject)
                }
            }
        }

        return constructPath(startCellCoord, targetCellCoord)
    }

    return {
        init,
        findPath,
        getAccessiblesCellsFrom
    }
})
