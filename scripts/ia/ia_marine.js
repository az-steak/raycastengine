define(['entities/base_entity', 'math/Vector2', 'utils/pathfinding'], function (BaseEntity, Vector2, Pathfinding) {

    var player

    function addCapabilities (object) {
        BaseEntity.addCapabilities(object)

        object.stateChangeTimeout

        object.hurtSound = "marine/hurt"
        object.deathSound = "marine/death"
        object.idleSound = "marine/idle"
        object.sightSound = "marine/sight"
        object.attackSound = "marine/attack"

        object.health = 100
        object.isDead = false

        object.moveSpeed = 0.1

        object.damage = 10
        object.fireRate = 1
        object.fireRateCount = 1

        object.idleBarkMaxDistance = 6
        object.idleBarkProbability = .002
        object.idleWanderProbability = 0.01

        object.update = function () {
            if (player == null) player = BaseEntity.getEntityById("Player")
            if (object.doAction != null) object.doAction()
        }




        /// STATE MACHINE
        object.doAction
        object.path

        object.doActionIdle = function () {
            if (Math.random() < object.idleBarkProbability && Vector2.distance(player.getV2Position(), object.getV2Position()) < object.idleBarkMaxDistance) object.playSoundOnLocation(object.idleSound)
            if (Math.random() < object.idleWanderProbability) {
                object.setModeWander()
            }
        }

        object.pathIndex = 0

        object.doActionWander = function () {
            var currentTarget = object.path[object.pathIndex]

            object.goTowards(currentTarget, object.moveSpeed)

            if (Vector2.distance(currentTarget, object) < 0.1) {
                object.pathIndex++
            }

            if (object.pathIndex >= object.path.length) {
                object.SetModeIdle()
            }
        }


        object.setModeWander = function () {
            var currentPos = object.getV2Position()
            var accessiblesCells = Pathfinding.getAccessiblesCellsFrom(currentPos, 3)
            var targetPos = accessiblesCells[Math.floor(Math.random() * accessiblesCells.length)]

            object.pathIndex = 0
            object.path = Pathfinding.findPath(currentPos, targetPos)

            object.doAction = object.doActionWander
        }

        object.SetModeIdle = function () {
            object.path.length = 0

            object.doAction = object.doActionIdle
        }

        object.doAction = object.doActionIdle

        ///









        object.onDamage = function (damage) {
            if (object.health <= 0) {
                object.die()
            } else if (object.states["hurt"]) {
                object.state = "hurt"
                object.changeStateAfter("idle", 500)
            }
        }

        object.onAnimEnd = function () {
            if (object.state == "death") object.state = "dead"
        }


        object.die = function () {
            object.cancelChangeStateAfter()
            object.health = 0
            object.isDead = true
            object.walkable = true
            object.state = "death"
            object.doAction = null
        }


        object.changeStateAfter = function (newState, delay) {
            if (object.stateChangeTimeout != null) {
                object.cancelChangeStateAfter()
            }

            object.stateChangeTimeout = setTimeout(function () {
                object.state = newState
                object.stateChangeTimeout = null
            }, delay)
        }

        object.cancelChangeStateAfter = function () {
            clearTimeout(object.stateChangeTimeout)
            object.stateChangeTimeout = null
        }
    }

    return {addCapabilities}
})
