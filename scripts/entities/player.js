define(["time", "entities/base_entity", "utils/base_audio_listener", "math/Vector2", "view/hud", "weapons/weapon_manager"],
function (Time, BaseEntity, BaseAudioListener, Vector2, HUD, WeaponManager) {

    var speedRedux = 0.4

    function Player (level, params, controller) {
        this.id = "Player"

        BaseEntity.addCapabilities(this)
        BaseAudioListener.addCapabilities(this)

        this.radius = 1

        this.renderable = false
        this.weaponOffsetY = 0
        this.health = 100

        this.init(params)
        this.rotationSpeed = 7
        this.speed = 12
        this.strafeSpeed = 8
        this.controller = controller
        this.level = level
        this.weaponManager = new WeaponManager(this)
        this.hud = new HUD()
        this.bobbingCount = 0

        this.changeWeapon(1)

        // var res = $('#resolution').val().split('x')
        this.ctx = $('#render')[0].getContext('2d')

        this.resolution = {
            x: this.ctx.canvas.width,
            y: this.ctx.canvas.height
        }
    }

    Player.prototype.init = function (params) {
        this.x           = params.x || 0.5,
        this.y           = params.y || 0.5,
        this.fov         = params.fov || Math.PI / 2
        this.orientation = params.orientation || 0
        this.radius      = params.radius || 0.2
    }

    Player.prototype.getMapCoords = function () {
        return {
            x: Math.floor(this.x),
            y: Math.floor(this.y)
        }
    }

    Player.prototype.getCoords = function () {
        return {
            x: this.x,
            y: this.y
        }
    }

    Player.prototype.getAngleInView = function (target) {
        direction = new Vector2(target.x - this.x, target.y - this.y)
        direction.normalize()

        return Math.acos(Vector2.dot(direction, this.getForward()))
    }

    Player.prototype.update = function () {
        this.controller.update()
        this.controls()
        this.handleWeaponSwap()

        if (this.weapon.update != undefined) {
            this.weapon.update()
        }
        if (this.nextWeapon != undefined) {
            this.swapWeaponAnim()
        }

        this.updateHud()
    }

    Player.prototype.swapWeaponAnim = function () {
        var direction = (this.weapon ==  this.weaponManager.weapons[this.nextWeapon]) ? -1 : 1
        this.weaponOffsetY += Time.deltaTime() * 4 * this.resolution.y * direction

        if (this.weaponOffsetY > this.resolution.y / 3) {
            this.weapon = this.weaponManager.weapons[this.nextWeapon]
        } else if (this.weaponOffsetY < 0) {
            this.weaponOffsetY = 0
            this.nextWeapon = undefined
        }
    }

    Player.prototype.changeWeapon = function (id) {
        if (this.weapon == undefined) {
            this.weapon = this.weaponManager.weapons[id]
        } else {
            this.nextWeapon = id
        }
    }

    Player.prototype.updateHud = function () {
        this.hud.health = this.health
        this.hud.ammo = this.weapon.ammo
    }

    Player.prototype.controls = function () {
        var dX = Math.cos(this.orientation) * this.controller.vertical() * this.speed
        var dY = Math.sin(this.orientation) * this.controller.vertical() * this.speed

        dX += Math.cos(this.orientation + Math.PI / 2) * this.controller.strafe() * this.strafeSpeed
        dY += Math.sin(this.orientation + Math.PI / 2) * this.controller.strafe() * this.strafeSpeed

        var realDeltaX = dX * speedRedux * Time.deltaTime()
        var realDeltaY = dY * speedRedux * Time.deltaTime()


        this.x += realDeltaX
        this.y += realDeltaY

        if (this.controller.rawVertical() != 0 || this.controller.rawStrafe() != 0) {
            this.bobbingCount += Time.deltaTime() * 40
        } else {
            this.bobbingCount = 0
        }

        if (this.controller.fire()) {
            this.weapon.fire()
        }

        this.orientation += this.controller.horizontal() * speedRedux * this.rotationSpeed * Time.deltaTime()
    }


    Player.prototype.handleWeaponSwap = function () {
        if (this.controller.weaponSlot1()) {
            this.changeWeapon(0)
        } else if (this.controller.weaponSlot2()) {
            this.changeWeapon(1)
        }
    }


    Player.prototype.getEntitiesInField = function (fieldOfHit) {
        var entities = BaseEntity.entities
        var entity
        var viewDir = this.getForward()
        var direction
        var distance
        var halfField = (fieldOfHit / 180 * Math.PI) / 2
        var entitiesInView = []


        for (var i in entities) {
            entity = entities[i]

            direction = new Vector2(entity.x - this.x, entity.y - this.y)
            direction.normalize()
            angle = Math.acos(Vector2.dot(direction, viewDir))

            if (angle < halfField) {
                if (entity.isVisible) {
                    entitiesInView.push(entity)
                }
            }
        }

        return entitiesInView
    }

    Player.prototype.destroy = function () {
        this.controller.destroy()
    }


    function getTileAt (level, x, y) {
        var indexMap = Math.floor(x) + Math.floor(y) * level.size
        return level.ressources[level.map[indexMap]]
    }


    return Player
})
