define(["math/Vector2", "time", "audio"], function (Vector2, Time, Audio) {

    var orientations = [
        "front",
        "frontLeft",
        "frontRight",
        "back",
        "left",
        "backLeft",
        "backRight",
        "right"
    ]
    var entities = []
    var playerEntity

    function addCapabilities (object) {
        entities.push(object)

        object.id = object.id || "entity"

        /// POSITION
        object.x = object.x || 0
        object.y = object.y || 0

        object.orientation = object.orientation || 0

        object.radius = object.radius || 0.2
        object.walkable = object.walkable || false


        object.onCollision = function (other) {}

        object.getV2GridPosition = function () {
            return new Vector2(Math.floor(object.x), Math.floor(object.y))
        }

        object.getV2Position = function () {
            return new Vector2(object.x, object.y)
        }

        object.getV2CellPosition = function () {
            var pos = object.getV2Position()
            pos.x = pos.x - Math.floor(pos.x)
            pos.y = pos.y - Math.floor(pos.y)

            return pos
        }


        object.getForward = function () {
            return new Vector2(
                Math.cos(object.orientation),
                Math.sin(object.orientation)
            )
        }
        object.getViewDirectionToPlayer = function () {
            var player = getPlayer()
            var angle = Math.atan2(player.y - object.y, player.x - object.x) + object.orientation
            return loopBetween(angle, -Math.PI, Math.PI)
        }
        object.getSideFacingPlayer = function () {
            var angle = object.getViewDirectionToPlayer()
            var side = Math.round(angle / (Math.PI / 4))

            var face

            switch (side) {
                case -4:  face = 'back'; break;
                case -1: face = 'frontLeft'; break;
                case -2:  face = 'left'; break;
                case -3:  face = 'backLeft'; break;
                case 3:  face = 'backRight'; break;
                case 2:  face = 'right'; break;
                case 1:  face = 'frontRight'; break;
                case 4:  face = 'back'; break;
                default: face = 'front'; break;
            }

            return face
        }
        object.goTowards = function (target, speed) {
            var direction = Vector2.minus(target, object).normalized()
            direction.multiply(speed * Time.deltaTime())


            object.x += direction.x
            object.y += direction.y
        }
        ///

        /// DAMAGES
        object.health = object.health || -1

        object.onDamage = function (damage) {}

        object.doDamage = function (damage) {
            if (object.health <= 0) {return}
            object.health -= damage

            if (object.health > 0) {
                object.playHurtSound()
            } else {
                object.playDeathSound()
            }

            object.onDamage(damage)
        }
        ///

        /// SOUNDS
        object.hurtSound = object.hurtSound || ""
        object.deathSound = object.deathSound || ""

        object.playHurtSound = function () {
            if (object.hurtSound != "") Audio.playSoundAtLocation("effects/" + object.hurtSound, {x:object.x, y: object.y})
        }
        object.playDeathSound = function () {
            if (object.deathSound != "") Audio.playSoundAtLocation("effects/" + object.deathSound, {x:object.x, y: object.y})
        }
        object.playSoundOnLocation = function (event) {
            Audio.playSoundAtLocation("effects/" + event, {x:object.x, y: object.y})
        }

        var position = {x:object.x, y: object.y}


        ///


        /// RENDERING
        object.state = "idle"
        object.isVisible = true
        object.renderable = true

        object.animSpeed = 0.5
        object.animationCount = 0
        object.animFrame = 0
        object.animTime = 0

        object.loadTextures = function (paths) {
            object.textures = {}
            object.states = paths.states
            object.basePath = './res/' + paths.basePath

            var stateId
            var fullPath
            var variant = ""
            var animation
            var hasAnimation

            if (paths.states == undefined) {
                object.textures["default"] = $('<img src="' + object.basePath + '.png"></img>')[0]
            }

            for (var state in paths.states) {
                animation = paths.states[state].animation
                stateId = "_" + state
                variant = ""
                hasAnimation = animation != undefined && animation > 0

                if (paths.states[state].variant != undefined) {
                    variant = paths.states[state].variant
                }

                if (paths.states[state].isOriented) {
                    for (var i in orientations) {
                        var subPath
                        if (hasAnimation) {
                            for (var x = 0; x < paths.states[state].animation + 1; x++) { // load every frame for every rotation
                                subPath = stateId + variant + x + "_" + orientations[i]
                                fullPath = object.basePath + subPath
                                object.textures[subPath] = $('<img src="' + fullPath + '.png"></img>')[0]
                            }
                        } else { // load every rotation of the state
                            subPath = stateId + variant + "_" + orientations[i]
                            fullPath = object.basePath + subPath
                            object.textures[subPath] = $('<img src="' + fullPath + '.png"></img>')[0]
                        }
                    }
                } else {
                    if (hasAnimation) {
                        for (var x = 0; x < paths.states[state].animation + 1; x++) { // load every frame of the state
                            fullPath = object.basePath + stateId + variant + x
                            object.textures[stateId + x] = $('<img src="' + fullPath + '.png"></img>')[0]
                        }
                    } else { //load the only frame for this state
                        fullPath = object.basePath + stateId + variant
                        object.textures[stateId] = $('<img src="' + fullPath + '.png"></img>')[0]
                    }
                }
            }
        }
        object.getTexture = function () {
            object.orientation %= Math.PI * 2
            if (object.orientation < 0) {object.orientation += Math.PI * 2}

            if (!object.renderable) { return }

            if (object.states == undefined) {
                return object.textures["default"]
            } else {
                if (object.states[object.state] == undefined) {
                    console.warn("State '" + object.state + "' doesn't exist")
                    // return
                }

                var spriteId = "_" + object.state
                if (object.isCurrentStateAnimated()) {
                    object.handleAnimation()
                    spriteId += object.animFrame
                } else {
                    object.animationCount = 0
                    object.animFrame = 0
                }
                if (object.states[object.state].isOriented) {
                    spriteId += "_" + object.getSideFacingPlayer()
                }
            }

            if (object.textures[spriteId] == undefined) console.log(spriteId);
            return object.textures[spriteId]
        }
        object.handleAnimation = function () {
            object.animationCount += Time.deltaTime() / object.animSpeed
            object.animTime += Time.deltaTime()

            if (object.animationCount >= object.states[object.state].animation) {
                object.onAnimEnd()
            } else {
                object.animationCount %= object.states[object.state].animation
            }

            object.animFrame = Math.round(object.animationCount)
        }
        object.isCurrentStateAnimated = function () {
            return object.states[object.state].animation != undefined && object.states[object.state].animation > 0
        }

        object.onAnimEnd = function () {}
        ///
    }

    function sortEntities () {
        entities.sort(function (a, b) {
            return distanceToPlayer(b) - distanceToPlayer(a)
        })
    }

    function distanceToPlayer (a) {
        var player = getPlayer()
        return Math.sqrt(Math.pow(a.x - player.x, 2) + Math.pow(a.y - player.y, 2))
    }

    function loopBetween (value, min, max) {
        if (value < min) {
            value += max - min
        } else if (value > max) {
            value -= max - min
        }
        return value
    }

    function getPlayer () {
        if (playerEntity == null) playerEntity = getEntityById("Player")

        return playerEntity
    }

    function getEntityById (id) {
        var result = null

        for (var i in entities) {
            if (entities[i].id == id) {
                result = entities[i]
                break;
            }
        }
        return result
    }


    function destroy () {
        entities.length = 0
    }



    return {
        addCapabilities,
        entities,
        getEntityById,
        sortEntities,
        destroy
    }
})
