define([], function () {

    var mouseSensitivity = 0.5

    function Controller () {
        this.prevKeys = {}
        this.prevValues = {}

        this.keys = {}
        this.values = {}

        initListeners(this)
    }

    var prevMouseX
    var mouseDeltaX = 0
    var lastMovementTimeStamp = 0

    var loop

    var moveInertia = 0.9
    var strafeInertia = 0.4

    var inertias = {
        90: moveInertia, // z
        83: moveInertia, // s
        65: moveInertia, // q
        69: moveInertia, // d
        81: strafeInertia, // a
        68: strafeInertia, // e
    }

    function initListeners (controller) {
        $(window).keydown(function (event) {
            controller.keyPressed(event)
        })
        $(window).keyup(function (event) {
            controller.keyReleased(event)
        })


        loop = setInterval(this.update, 10)
    }

    Controller.prototype.update = function () {
        mouseDeltaX = 0
    }

    Controller.prototype.keyPressed = function (event) {
        // console.log(event.keyCode);
        if (!this.keys[event.keyCode]) this.prevKeys[event.keyCode] = true
        this.keys[event.keyCode] = true
    }

    Controller.prototype.update = function() {
        for (var key in this.keys) {
            var value = this.values[key] || 0
            var direction = this.keys[key] ? 1 : 0
            var inertia = inertias[key]

            this.values[key] = (inertia * value + (1 - inertia) * direction) / (inertia + (1 - inertia))
        }
    }

    Controller.prototype.keyReleased = function (event) {
        this.prevKeys[event.keyCode] = false
        this.keys[event.keyCode] = false
    }

    Controller.prototype.getRawKey = function (key) {
        return this.keys[key] ? 1 : 0
    }

    Controller.prototype.getKey = function (key) {
        return this.values[key] || 0
    }

    Controller.prototype.getKeyDown = function (key) {
        var value = this.prevKeys[key]
        this.prevKeys[key] = false
        return value
    }

    Controller.prototype.vertical = function () {
        return this.getKey(90) - this.getKey(83)
    }

    Controller.prototype.strafe = function () {
        return - this.getKey(65) + this.getKey(69)
    }

    Controller.prototype.horizontal = function () {
        return (- this.getKey(81) + this.getKey(68)) * 0.5
    }

    Controller.prototype.rawVertical = function () {
        return this.getRawKey(90) - this.getRawKey(83)
    }

    Controller.prototype.rawStrafe = function () {
        return - this.getRawKey(65) + this.getRawKey(69)
    }

    Controller.prototype.rawHorizontal = function () {
        return (- this.getRawKey(81) + this.getRawKey(68)) * 0.5
    }

    Controller.prototype.fire = function () {
        return this.getRawKey(13)
    }

    Controller.prototype.weaponSlot1 = function () {
        return this.getRawKey(49)
    }

    Controller.prototype.weaponSlot2 = function () {
        return this.getRawKey(50)
    }

    Controller.prototype.weaponSlot3 = function () {
        return this.getRawKey(51)
    }

    Controller.prototype.weaponSlot4 = function () {
        return this.getRawKey(52)
    }


    Controller.prototype.pause = function () {
        return this.getKeyDown(27)
    }





    Controller.prototype.destroy = function () {
        $(window).off('keydown')
        $(window).off('keyup')
        clearInterval(loop)
    }


    return Controller
})
