define(['audio'], function (Audio) {

    var pauseScreen = $('#pause.ui')
    var optionsScreen = $('#pauseOptions.ui')
    var confirmScreen = $('#pauseConfirm.ui')
    var callback
    var confirmCallback

    var restartCallback
    var quitCallback

    var resumeButton = $('#resumeButton')
    var restartButton = $('#restartButton')
    var optionsButton = $('#pauseOptionsButton')
    var quitButton = $('#quitButton')




    /// OPTIONS BUTTONS
    var optionsBackButton = $('#pauseQuitOptions')

    var mainVolumeSetup = $("#pauseMainVolume")
    var musicVolumeSetup = $("#pauseMusicVolume")
    var fxVolumeSetup = $("#pauseFxVolume")

    /// CONFIRM BUTTONS
    var yesButton = $('#yesButton')
    var noButton = $('#noButton')

    function init () {
        initListeners()
        closeMenu()
    }

    function initListeners () {
        resumeButton.click(function () {
            callback()
            callback = undefined
        })

        optionsButton.click(openOptions)
        optionsBackButton.click(closeOptions)

        restartButton.click(function () {
            openConfirm(restartCallback)
        })
        quitButton.click(function () {
            openConfirm(quitCallback)
        })


        $('.uiButton').mouseover(function () {
            Audio.playSound('effects/ui/beep')
        })

        $('.uiButton').click(function () {
            Audio.playSound('effects/ui/switch_on', 0.3)
        })


        mainVolumeSetup.on('input', function() {
            applyVolumes()
            Audio.playSound('effects/ui/beep')
        })

        musicVolumeSetup.on('input', function() {
            applyVolumes()
        })

        fxVolumeSetup.on('input', function() {
            applyVolumes()
            Audio.playSound('effects/ui/beep')
        })

        yesButton.click(function () {
            closeMenu()
            confirmCallback()
            confirmCallback = undefined
        })

        noButton.click(function () {
            closeConfirm()
            confirmCallback = undefined
        })
    }

    function openMenu (closeCallback, pRestartCallback, pQuitCallback) {
        callback = closeCallback
        restartCallback = pRestartCallback
        quitCallback = pQuitCallback
        pauseScreen.show()
    }

    function closeMenu () {
        closeConfirm()
        closeOptions()
        pauseScreen.hide()
    }


    function openConfirm (callback) {
        confirmCallback = callback
        confirmScreen.show()
    }

    function closeConfirm () {
        confirmScreen.hide()
    }



    function openOptions() {
        loadOptions()
        optionsScreen.show()
    }

    function closeOptions() {
        saveOptions()
        optionsScreen.hide()
    }


    function applyVolumes () {
        Audio.setMainVolume(mainVolumeSetup.val())
        Audio.setMusicVolume(musicVolumeSetup.val())
        Audio.setFXVolume(fxVolumeSetup.val())
    }


    function saveOptions () {
        var settings = {
            volumes: {
                main: mainVolumeSetup.val(),
                music: musicVolumeSetup.val(),
                fx: fxVolumeSetup.val()
            }
        }

        localStorage.setItem('settings', JSON.stringify(settings))
    }

    function loadOptions () {
        var settings = JSON.parse(localStorage.getItem('settings'))

        if (settings != null) {
            mainVolumeSetup.val(settings.volumes.main)
            musicVolumeSetup.val(settings.volumes.music)
            fxVolumeSetup.val(settings.volumes.fx)
        }

        applyVolumes()
    }


    return {
        init,
        openMenu,
        closeMenu,
    }
})
