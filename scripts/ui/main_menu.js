define(['audio'], function (Audio) {

    var mainScreen = $('#mainMenu.ui')
    var levelSelectionScreen = $('#levelSelection.ui')
    var optionsScreen = $('#mainOptions.ui')
    var loadScreen = $('#imgMelt.ui')

    var startButton = $('#newGameButton')
    var loadButton = $('#loadButton')
    var optionsButton = $('#mainOptionsButton')
    var creditsButton = $('#creditsButton')


    /// LEVEL SELECT BUTTONS
    var levelSelectButtons = $('.levelSelect')
    var quiLevelSelectButton = $('#quitLevelSelect')

    /// OPTIONS BUTTONS
    var optionsBackButton = $('#mainQuitOptions')

    var mainVolumeSetup = $("#mainVolume")
    var musicVolumeSetup = $("#musicVolume")
    var fxVolumeSetup = $("#fxVolume")


    var startCallback


    function init (startGameCallback) {
        startCallback = startGameCallback

        loadOptions()
        applyVolumes()
        initListeners()
        closeMenu()
    }

    function initListeners () {
        startButton.click(openLevelSelect)
        quiLevelSelectButton.click(closeLevelSelect)
        optionsButton.click(openOptions)
        optionsBackButton.click(closeOptions)

        $('.uiButton').mouseover(function () {
            Audio.playSound('effects/ui/beep')
        })

        $('.uiButton').click(function () {
            Audio.playSound('effects/ui/switch_on', 0.3)
        })



        mainVolumeSetup.on('input', function() {
            applyVolumes()
            Audio.playSound('effects/ui/beep')
        })

        musicVolumeSetup.on('input', function() {
            applyVolumes()
        })

        fxVolumeSetup.on('input', function() {
            applyVolumes()
            Audio.playSound('effects/ui/beep')
        })
    }

    function initLevelSelectListeners (maps) {
        levelSelectButtons = $('.levelSelect')

        levelSelectButtons.click(function (event) {
            var mapName = event.target.dataset['mapname']
            startGame(maps[mapName])
        })
    }

    function setLevelSelection (maps) {
        var buttonsContainer = levelSelectionScreen.find("#selectMenu")

        for (mapName in maps) {
            var button = $("<li class='uiButton levelSelect' data-mapname='" + mapName + "'>" + mapName + "</li>")
            buttonsContainer.append(button)
        }

        initLevelSelectListeners(maps)
    }


    function startGame (lvlToLoad) {
        closeMenu()
        loadScreen.css('top', 0)
        loadScreen.show()
        startCallback(lvlToLoad, true)
    }


    function openMenu () {
        Audio.playMusic('musics/d_inter', true)
        mainScreen.show()
        loadScreen.hide()
    }

    function closeMenu () {
        Audio.stopMusic('musics/d_inter')
        closeOptions()
        closeLevelSelect()
        mainScreen.hide()
    }

    function openLevelSelect () {
        levelSelectionScreen.show()
    }

    function closeLevelSelect () {
        levelSelectionScreen.hide()
    }

    function openOptions() {
        loadOptions()
        optionsScreen.show()
    }

    function closeOptions() {
        saveOptions()
        optionsScreen.hide()
    }

    function closeLoadScreen() {
        var pos = 0
        var speed = 10

        var anim = setInterval(function () {
            loadScreen.css('top', pos)
            pos += speed

            if (pos > 2000) {
                loadScreen.css('top', 0)
                loadScreen.hide()
                clearInterval(anim)
            }
        })
    }




    function applyVolumes () {
        Audio.setMainVolume(mainVolumeSetup.val())
        Audio.setMusicVolume(musicVolumeSetup.val())
        Audio.setFXVolume(fxVolumeSetup.val())
    }


    function saveOptions () {
        var settings = {
            volumes: {
                main: mainVolumeSetup.val(),
                music: musicVolumeSetup.val(),
                fx: fxVolumeSetup.val()
            }
        }

        localStorage.setItem('settings', JSON.stringify(settings))
    }


    function loadOptions () {
        var settings = JSON.parse(localStorage.getItem('settings'))

        if (settings != null) {
            mainVolumeSetup.val(settings.volumes.main)
            musicVolumeSetup.val(settings.volumes.music)
            fxVolumeSetup.val(settings.volumes.fx)
        }

        applyVolumes()
    }

    return {
        init,
        setLevelSelection,
        openMenu,
        closeMenu,
        closeLoadScreen
    }
})
