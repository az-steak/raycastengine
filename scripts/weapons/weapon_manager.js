define(["weapons/finger", "weapons/colt"], function (Finger, Colt) {

    function WeaponManager (player) {
        this.weapons = []

        this.weapons.push(new Finger(player))
        this.weapons.push(new Colt(player))
    }


    return WeaponManager
})
