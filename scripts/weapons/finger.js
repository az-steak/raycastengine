define(["weapons/weapon"], function (AddWeaponCapabilities) {

    var spritePath = "res/sprites/weapons/finger/Finger"

    function Weapon (owner) {
        this.owner = owner
        this.texture = spritePath

        this.fireRate = 4
        this.damage = 50
        this.fieldOfFire = 20
        this.ammo = 10

        AddWeaponCapabilities(this)
    }


    return Weapon
})
