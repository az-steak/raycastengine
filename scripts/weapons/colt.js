define(["weapons/weapon"], function (AddWeaponCapabilities) {

    var spritePath = "res/sprites/weapons/colt/colt"

    function Weapon (owner) {
        this.owner = owner
        this.texture = spritePath

        this.fireRate = 10
        this.damage = 20
        this.fieldOfFire = 6
        this.ammo = 200

        AddWeaponCapabilities(this)
    }


    return Weapon
})
