define(["time", "math/Vector2", "world/physics", "audio"], function (Time, Vector2, Physics, Audio) {

    function addCapabilities(weapon) {
        weapon.lastFireTime = 0
        weapon.timeShooting = 0

        weapon.ammo = weapon.ammo || 0

        weapon.textures = {
            default: $('<img src="' + weapon.texture + '.png"></img>')[0],
            fire: $('<img src="' + weapon.texture + '_fire.png"></img>')[0]
        }

        weapon.update = function () {
            weapon.lastFireTime += Time.deltaTime()
            weapon.shootingDuration -= Time.deltaTime()
        }

        weapon.fire = function () {
            if (weapon.lastFireTime < 1/weapon.fireRate) {return}

            if (weapon.ammo == 0) {
                return
            }

            weapon.ammo--

            Audio.playSound("effects/finger/fire")
            var hit = Physics.raycast(weapon.owner, weapon.owner.getForward())

            // console.log(hit.hit.point);

            weapon.lastFireTime = 0
            weapon.shootingDuration = 0.1

            var targets = weapon.owner.getEntitiesInField(weapon.fieldOfFire)

            if (targets.length == 0) {return}

            var nearestTarget = targets[0]
            var minDistance = Vector2.distance(weapon.owner, targets[0])

            for (var i in targets) {
                var target = targets[i]
                var distance = Vector2.distance(weapon.owner, target)

                if (distance < minDistance && target.doDamage != undefined && !target.isDead) {
                    minDistance = distance
                    nearestTarget = target
                }

            }
            if (nearestTarget.doDamage != undefined) {
                nearestTarget.doDamage(weapon.damage)
            }
        }

        weapon.getTexture = function () {
            var id

            if (weapon.shootingDuration > 0) {
                id = "fire"
            } else {
                id = "default"
            }

            return weapon.textures[id]
        }
    }


    return addCapabilities
})
