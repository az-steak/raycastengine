define(["math/Vector2", "time", "entities/base_entity"], function (Vector2, Time, Entity) {


    var instance

    function Physics (level, entities) {
        this.level = level
        this.gridSize = new Vector2(level.size, level.size)
        this.grid = []
        this.lastKnownEntitiesIndex = {}
        this.newEntitiyPos = {}


        // Init grid
        for (var y = 0; y < this.gridSize.y; y++) {
            for (var x = 0; x < this.gridSize.x; x++) {
                this.grid.push([])
            }
        }

        //Init entities in grid
        for (entity of entities) {
            var index = this.getIndexFromV2(entity.getV2Position())

            this.grid[index].push(entity)
            this.lastKnownEntitiesIndex[entity.id] = index
        }

        instance = this
    }



    Physics.prototype.compute = function ()  {
        for (entity of Entity.entities) {
            this.checkCollisionWithEntities(entity)
            this.checkWallsCollision(entity)
        }

        this.applyNewPositions()
    }



    Physics.prototype.checkCollisionWithEntities = function (entity) {
        var index = this.getIndexFromV2(entity.getV2Position())
        var prevIndex = this.lastKnownEntitiesIndex[entity.id]

        this.lastKnownEntitiesIndex[entity.id] = index

        if (index != prevIndex) {
            var indexToRemove = this.grid.indexOf(entity)
            this.grid[prevIndex].splice(indexToRemove, indexToRemove)
            this.grid[index].push(entity)
        }


        var neighbourhoodCells = this.getNeighboursIndexes(index)

        for (neighbourIndex of neighbourhoodCells) {
            for (otherEntity of this.grid[neighbourIndex]) {
                if (otherEntity != entity) {

                    var distance = Vector2.distance(entity.getV2Position(), otherEntity.getV2Position())

                    if (distance < entity.radius + otherEntity.radius) {
                        entity.onCollision(otherEntity)
                        otherEntity.onCollision(entity)

                        if (!otherEntity.walkable && !entity.walkable) {
                            this.collide(entity, otherEntity)
                        }
                    }
                }
            }
        }
    }



    Physics.prototype.collide = function (collided, other) {
        var pos1 = collided.getV2Position()
        var pos2 = other.getV2Position()

        var distance = Vector2.distance(pos1, pos2)
        var repelDirection = Vector2.minus(pos1, pos2).normalized()
        repelDirection.multiply(collided.radius + other.radius)

        // console.log("Current distance = " + distance + "   Target distance : " + (collided.radius + other.radius) + "   RepelV magnitude : " + newPos.magnitude());
        var newPos = Vector2.add(pos2, repelDirection)

        this.newEntitiyPos[collided.id] = {
            object: collided,
            newPos
        }
    }



    Physics.prototype.checkWallsCollision = function (entity) {
        var localPos = entity.getV2CellPosition()
        var gridPos = entity.getV2GridPosition()
        var currentTile = this.level.getTileAt(gridPos.x, gridPos.y)
        var nextTile

        if (localPos.y > 1 - entity.radius) {
            nextTile = this.level.getTileAt(entity.x, entity.y + 1)

            if (!nextTile.walkable) {
                entity.y = (gridPos.y + 1) - entity.radius
            }
        } else if (localPos.y < entity.radius) {
            nextTile = this.level.getTileAt(entity.x, entity.y - 1)

            if (!nextTile.walkable) {
                entity.y = gridPos.y + entity.radius
            }
        }


        if (localPos.x > 1 - entity.radius) {
            nextTile = this.level.getTileAt(entity.x + 1, entity.y)

            if (!nextTile.walkable) {
                entity.x = (gridPos.x + 1) - entity.radius
            }
        } else if (localPos.x < entity.radius) {
            nextTile = this.level.getTileAt(entity.x - 1, entity.y)

            if (!nextTile.walkable) {
                entity.x = gridPos.x + entity.radius
            }
        }

        if (!currentTile.walkable) {
            var sum = localPos.x + localPos.y
            if (sum < 1 && localPos.x > localPos.y) {
                entity.y -= localPos.y + entity.radius
            } else if (sum < 1 && localPos.x < localPos.y) {
                entity.x -= localPos.x + entity.radius
            } else if (sum > 1 && localPos.x > localPos.y) {
                entity.x += localPos.x + entity.radius
            } else if (sum > 1 && localPos.x < localPos.y) {
                entity.y += localPos.y + entity.radius
            }
        }
    }



    Physics.prototype.applyNewPositions = function () {
        for (entityId in this.newEntitiyPos) {
            var object = this.newEntitiyPos[entityId].object
            var newPos = this.newEntitiyPos[entityId].newPos

            entity.x = newPos.x
            entity.y = newPos.y
        }

        if (Object.keys(this.newEntitiyPos).length > 0) {
            this.newEntitiyPos = {}
        }
    }



    Physics.prototype.getCell = function (coords) {
        var index = this.getIndexFromV2(coords)
        return this.level.ressources[this.level.map[index]]
    }



    Physics.prototype.getNeighboursIndexes = function (index) {
        return [
            index - this.gridSize.x - 1,
            index - this.gridSize.x,
            index - this.gridSize.x + 1,
            index - 1,
            index,
            index + 1,
            index + this.gridSize.x - 1,
            index + this.gridSize.x,
            index + this.gridSize.x + 1
        ]
    }

    Physics.prototype.getIndexFromV2 = function (coords) {
        return Math.floor(coords.x) + Math.floor(coords.y) * this.gridSize.x
    }



    // STATIC FCTS

    Physics.raycast = function (origin, direction, maxDistance = Number.POSITIVE_INFINITY) {
        return Physics.raycast(origin, direction, maxDistance, true)
    }


    Physics.raycast = function (origin, direction, maxDistance, ignoreEntities) {
        var castData = getCastData(origin, direction)
        var gridCoords = castData.gridCoords
        var sideDist = castData.sideDist
        var deltaDist = castData.deltaDist
        var steps = castData.steps

        var raycastResult = {}
        var currentCell
        var hitted = false
        var side = 0 // side of cube touched  0 : Y-oriented wall   1 : X-oriented wall
        var secureCount = 0
        var distance = deltaDist.x + deltaDist.y


        while (!hitted && secureCount < 1000) {
            if (sideDist.x < sideDist.y) {
                sideDist.x += deltaDist.x
                distance += deltaDist.x
                gridCoords.x += steps.x
                side = 0
            } else {
                sideDist.y += deltaDist.y
                distance += deltaDist.y
                gridCoords.y += steps.y
                side = 1
            }


            currentCell = instance.getCell(gridCoords)

            if (!currentCell.walkable) hitted = true

            secureCount++
        }

        var hitInfo

        if (hitted) {
            var normal = getNormalDir(direction, side)

            hitInfo = {
                point: getHitPoint(origin, direction, castData, side, gridCoords),
                normal,
                hitted: {
                    objectType: "cell",
                    object: currentCell
                }
            }
        }

        return {
            hit: hitInfo,
            origin: new Vector2(origin.x, origin.y),
            direction: new Vector2(direction.x, direction.y)
        }
    }





    Physics.prototype.destroy = function () {
        instance = null;
    }


    function getCastData (origin, direction) {
        var gridCoords = new Vector2(Math.floor(origin.x), Math.floor(origin.y))
        var deltaDist = new Vector2()

        deltaDist.x = Math.sqrt(1 + (direction.y ** 2) / (direction.x ** 2))
        deltaDist.y = Math.sqrt(1 + (direction.x ** 2) / (direction.y ** 2))


        var steps = new Vector2()
        var sideDist = new Vector2()

        if (direction.x < 0) {
            steps.x = -1
            sideDist.x = (origin.x - gridCoords.x) * deltaDist.x;
        } else
        {
            steps.x = 1;
            sideDist.x = (gridCoords.x + 1.0 - origin.x) * deltaDist.x;
        }

        if (direction.y < 0)
        {
            steps.y = -1;
            sideDist.y = (origin.y - gridCoords.y) * deltaDist.y;
        } else
        {
            steps.y = 1;
            sideDist.y = (gridCoords.y + 1.0 - origin.y) * deltaDist.y;
        }

        return {
            steps,
            sideDist,
            deltaDist,
            gridCoords
        }
    }

    function getNormalDir (direction, side) {
        if (side == 0) {
            if (direction.x > 0) {
                return Vector2.left()
            } else {
                return Vector2.right()
            }
        } else {
            if (direction.y < 0) {
                return Vector2.up()
            } else {
                return Vector2.down()
            }
        }
    }

    function getHitPoint (origin, direction, castData, side, gridCoords) {
        var hitCoords = new Vector2()

        if (side === 0) {
            distance = (castData.gridCoords.x - origin.x + (1 - castData.steps.x) / 2) / direction.x
            blocRelativeX = (origin.y + distance * direction.y)

            if (direction.x > 0) {
                hitCoords.x = gridCoords.x
            } else {
                hitCoords.x = gridCoords.x + 1
            }
            hitCoords.y = blocRelativeX

        } else {
            distance = (castData.gridCoords.y - origin.y + (1 - castData.steps.y) / 2) / direction.y
            blocRelativeX = (origin.x + distance * direction.x)

            if (direction.y > 0) {
                hitCoords.y = gridCoords.y
            } else {
                hitCoords.y = gridCoords.y + 1
            }
            hitCoords.x = blocRelativeX
        }

        return hitCoords
    }


    return Physics
})
