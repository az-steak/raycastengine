define(['jquery', 'entities/base_entity', 'entities/player', 'entities/controller'],
function ($, BaseEntity, Player, Controller, BaseIA) {

    var errorBlock = {
        walkable: false,
        transparent: false
    }

    var packageTextures

    function Level () {}

    Level.setPackageTextures = function (texturesData) {
        packageTextures = texturesData
    }

    Level.prototype.loadLevel = function (level, callback) {
        // var fullPath = './res/levels/' + levelName + '.json'
        var instance = this

        this.map = level.data
        this.spawnPoint = level.spawnPoint
        this.ressources = level.components
        this.player     = this.loadPlayer()
        this.size = level.size
        this.backdrop = level.backdrop || 'background'

        this.loadSounds(level)
        this.loadEntities(level.entities, callback)
    }

    Level.prototype.getTileAt = function (x, y) {
        var indexMap = Math.floor(x) + Math.floor(y) * this.size
        var name = this.map[indexMap]
        var result = this.ressources[name]


        if (!result) {
            result = errorBlock
        } else {
            result.name = name
        }

        return result
    }


    Level.prototype.loadPlayer = function () {
        var params = {
            x: this.spawnPoint.x,
            y: this.spawnPoint.y,
            orientation: this.spawnPoint.orientation,
            fov: Math.PI / 3
        }


        return new Player(this, params, new Controller())
    }

    Level.prototype.loadSounds = function (level) {
        if (level.backgroundMusic != undefined) {
            this.backgroundMusic = level.backgroundMusic
            // Sound.loadSound(level.backgroundMusic, 'backgroundMusic')
        }
    }

    Level.prototype.loadEntities = function (entities, callback) {
        this.entities = []

        if (entities == undefined || entities.length == 0) {
            callback()
        }

        for (i in entities) {
            var entity = entities[i]

            this.loadEntity(entities, entity, callback)
        }
    }

    Level.prototype.loadEntity = function (entities, entity, callback) {
        var instance = this

        if (entity.script == null) {
            initEntity(entity)

            instance.entities.push(entity)
            if (instance.entities.length == entities.length) {
                callback()
            }
        } else {
            require([entity.script + ".js"], function (Behaviour) {
                initEntity(entity, Behaviour)
                delete entity.script

                instance.entities.push(entity)
                if (instance.entities.length == entities.length) {
                    callback()
                }
            })
        }
    }


    function initEntity (entity, Behaviour) {
        BaseEntity.addCapabilities(entity)

        if (Behaviour != null) {
            Behaviour.addCapabilities(entity)
        }

        entity.loadTextures(entity.sprites)
    }

    return Level
})
