require.config({
    paths: {
        jquery: './ext_libs/jquery.min',
        audio:  './utils/audio'
    }
})



require(['jquery',
    'view/raycast', 'world/physics', 'world/level', 'entities/base_entity', 'time', 'math/Vector2', 'audio', 'utils/pathfinding',
    'ui/main_menu', 'ui/pause_menu'
],
function ($,
    Raycast, Physics, Level, Entity, Time, Vector2, Audio, Pathfinding,
    MainMenu, PauseMenu
) {

    var firstLaunch = true
    var engineRunning = false
    var playMeltAnimOnLoad
    var frameRate   = 35
    var resolutions = {
        '720x405'  : [720, 405],
        '1200x700' : [1200, 700]
    }

    var engine
    var physicEngine
    var loadedLevel
    var player

    var gamePaused
    var gameLoopInterval


    function init (package) {
        if (firstLaunch) {
            Audio.init()
            MainMenu.init(startEngine)
            PauseMenu.init()

            Audio.playSound('effects/shotgun/fire')
            firstLaunch = false
        }


        Raycast.setPackageData(package)
        MainMenu.setLevelSelection(package.maps)
        Level.setPackageTextures(package.textures)
        openMainMenu()
    }


    function startEngine (levelToLoad, fromMainMenu = false) {
        engineRunning = true
        gamePaused = false
        playMeltAnimOnLoad = fromMainMenu

        loadedLevel = new Level()
        loadedLevel.loadLevel(levelToLoad, loadResources)
    }

    function stopEngine () {
        if (!engineRunning) return

        engineRunning = false
        clearInterval(gameLoopInterval)


        player.destroy()
        physicEngine.destroy()
        Entity.destroy()

        engine.clearScreens()
        Audio.stopMusic()
    }

    function restartEngine () {
        stopEngine()
        startEngine()
    }

    function openMainMenu () {
        stopEngine()
        MainMenu.openMenu()
    }

    function loadResources () {
        if (playMeltAnimOnLoad) {
            MainMenu.closeLoadScreen()
        }
        Pathfinding.init(loadedLevel)

        engine = new Raycast($('#render'), $('#postprocess'), getResolutionParam(), loadedLevel)
        engine.loadTextures(loadedLevel)


        physicEngine = new Physics(loadedLevel, Entity.entities)


        player = loadedLevel.player


        gameLoopInterval = setInterval(function () {
            gameLoop(engine)
        }, 1000/frameRate)


        if (loadedLevel.backgroundMusic != undefined) {
            Audio.playMusic(loadedLevel.backgroundMusic, true)
        }

    }

    var prevTime = Date.now()

    function gameLoop() {
        if (!gamePaused) {
            Time.setDeltaTime((Date.now() - prevTime) / 1000)

            for (var i = Entity.entities.length - 1; i >= 0; i--) {
                var entity = Entity.entities[i]

                if (entity.update != null) {
                    entity.update()
                }
            }

            Audio.update()
            physicEngine.compute()
        } else {
            Time.setDeltaTime(0)
        }

        prevTime = Date.now()



        engine.render(loadedLevel, player)

        if (player.controller.pause()) togglePause()
    }

    function togglePause () {
        gamePaused = !gamePaused

        if (gamePaused) {
            PauseMenu.openMenu(togglePause, restartEngine, openMainMenu)
            Audio.playSound('effects/ui/switch_on', 0.3)
            // Audio.pauseMusic()
        } else {
            PauseMenu.closeMenu()
            Audio.playSound('effects/ui/switch_off', 0.3)
            // Audio.resumeMusic()
        }
    }

    function getResolutionParam () {
        //// TODO: Settable resolution
        return {
            x: 1280,
            y: 720
        }
    }

    function loadPackage (packageName, callback) {
        $.ajax({
            url: "./res/packages/" + packageName + ".rep",
            async: true,
            dataType: "json",
            success: function (package) {
                console.log(package)
                callback(package)
            }
        })
    }


    $(document).ready(function () {
        var titleScreen = $('#askInput')

        loadPackage("doomNew", function (package) {
            $('body').click(function () {
                $('body').off("click")
                titleScreen.hide()
                init(package)
            })
        })
    })
})
